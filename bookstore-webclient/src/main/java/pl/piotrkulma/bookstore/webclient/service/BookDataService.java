package pl.piotrkulma.bookstore.webclient.service;

import java.util.List;

import pl.piotrkulma.bookstore.dto.book.AuthorDTO;
import pl.piotrkulma.bookstore.webclient.dto.BookDataDTO;

public interface BookDataService {
	public AuthorDTO getAuthor(long id);
	public List<BookDataDTO> getAllBooks();
}
