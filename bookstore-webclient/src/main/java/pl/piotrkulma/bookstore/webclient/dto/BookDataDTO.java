package pl.piotrkulma.bookstore.webclient.dto;

import java.util.ArrayList;
import java.util.List;

import pl.piotrkulma.bookstore.dto.book.AuthorDTO;
import pl.piotrkulma.bookstore.dto.book.BookDTO;

public final class BookDataDTO {
	private BookDTO book;
	private List<AuthorDTO> authors;
	
	public static final class BookDataDTOBuilder {
		private BookDTO book;
		private List<AuthorDTO> authors;
		
		public static BookDataDTOBuilder getInstance() {
			return new BookDataDTOBuilder();
		}
		
		public BookDataDTO build() {
			BookDataDTO dto = new BookDataDTO();
			dto.book = book;
			dto.authors = new ArrayList<>();
			
			if(authors != null) {
				dto.authors.addAll(authors);
			}
			
			return dto;
		}
		
		public BookDataDTOBuilder withBook(BookDTO bookDto) {
			this.book = bookDto;
			return this;
		}
		
		public BookDataDTOBuilder withAuthors(List<AuthorDTO> authorsDto) {
			if(this.authors == null) {
				this.authors = new ArrayList<>();
			}
			
			this.authors.addAll(authorsDto);
			
			return this;
		}
		
		public BookDataDTOBuilder addAuthor(AuthorDTO authorDto) {
			if(this.authors == null) {
				this.authors = new ArrayList<>();
			}
			
			this.authors.add(authorDto);
			
			return this;
		}
		
		private BookDataDTOBuilder() {
		}
	}

	public BookDTO getBook() {
		return book;
	}

	public List<AuthorDTO> getAuthors() {
		return authors;
	}
	
	private BookDataDTO() {
	}
}
