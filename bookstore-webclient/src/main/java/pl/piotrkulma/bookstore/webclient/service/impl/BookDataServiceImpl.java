package pl.piotrkulma.bookstore.webclient.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.piotrkulma.bookstore.dto.book.AuthorDTO;
import pl.piotrkulma.bookstore.dto.book.BookDTO;
import pl.piotrkulma.bookstore.webclient.dto.BookDataDTO;
import pl.piotrkulma.bookstore.webclient.dto.BookDataDTO.BookDataDTOBuilder;
import pl.piotrkulma.bookstore.webclient.feignclient.AuthorFeignClient;
import pl.piotrkulma.bookstore.webclient.feignclient.BookFeignClient;
import pl.piotrkulma.bookstore.webclient.service.BookDataService;

@Service
public class BookDataServiceImpl implements BookDataService {
	protected AuthorFeignClient authorFeignClient;
	protected BookFeignClient bookFeignClient;
	
	@Autowired
	public BookDataServiceImpl(AuthorFeignClient authorFeignClient, BookFeignClient bookFeignClient) {
		this.authorFeignClient = authorFeignClient;
		this.bookFeignClient = bookFeignClient;
	}
	
	@Override
	public AuthorDTO getAuthor(long id) {
		return authorFeignClient.getAuthor(id).getBody();
	}
	
	@Override
	public List<BookDataDTO> getAllBooks() {		
		return bookFeignClient.getBooks().getBody().stream()
		.map(b -> createBookData(b))
		.collect(Collectors.toList());
	}
	
	protected BookDataDTO createBookData(BookDTO book) {
		BookDataDTOBuilder builder = BookDataDTO.BookDataDTOBuilder.getInstance();
				
		List<AuthorDTO> authors = book.getAuthorsIds().stream()
		.map(id -> authorFeignClient.getAuthor(id).getBody())
		.collect(Collectors.toList());
		
		return builder.withBook(book).withAuthors(authors).build();
	}
}
