package pl.piotrkulma.bookstore.webclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value= {
		"classpath:webclient.properties"
		})
@ComponentScan(value= {
		"pl.piotrkulma.bookstore.webclient.controller",
		"pl.piotrkulma.bookstore.webclient.feignclient",
		"pl.piotrkulma.bookstore.webclient.service.impl"
		})
@EnableFeignClients
@SpringBootApplication
public class ApplicationConfig {
	public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
	}
}
