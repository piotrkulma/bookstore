package pl.piotrkulma.bookstore.webclient.feignclient;

import org.springframework.cloud.openfeign.FeignClient;

import pl.piotrkulma.bookstore.bookservice.rest.BookInfoApi;

@FeignClient("BOOKSTORE-BOOKSERVICE")
public interface BookFeignClient extends BookInfoApi{

}
