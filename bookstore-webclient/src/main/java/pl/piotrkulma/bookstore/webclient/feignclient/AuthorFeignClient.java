package pl.piotrkulma.bookstore.webclient.feignclient;

import org.springframework.cloud.openfeign.FeignClient;

import pl.piotrkulma.bookstore.authorservice.rest.AuthorInfoApi;

@FeignClient("BOOKSTORE-AUTHORSERVICE")
public interface AuthorFeignClient extends AuthorInfoApi {

}
