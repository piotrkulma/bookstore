package pl.piotrkulma.bookstore.webclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pl.piotrkulma.bookstore.webclient.service.BookDataService;

@Controller
public class BookController {
	protected BookDataService bookDataService;
	
	@Autowired
	public BookController(BookDataService bookDataService) {
		this.bookDataService = bookDataService;
	}

	@GetMapping("/author")
	public String author(@RequestParam(name="id", required=true) long id, Model model) {		
        model.addAttribute("author", bookDataService.getAuthor(id));
		return "author";
	}

	@GetMapping("/books")
	public String books(Model model) {		
        model.addAttribute("books", bookDataService.getAllBooks());
		return "books";
	}
}
