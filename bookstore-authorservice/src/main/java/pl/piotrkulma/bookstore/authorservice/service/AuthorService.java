package pl.piotrkulma.bookstore.authorservice.service;

import java.util.List;

import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

public interface AuthorService {
	AuthorDTO getAuthor(long id);
	List<AuthorDTO> getAllAuthors();
	AuthorDTO createAuthor(AuthorDTO authorDTO);
}
