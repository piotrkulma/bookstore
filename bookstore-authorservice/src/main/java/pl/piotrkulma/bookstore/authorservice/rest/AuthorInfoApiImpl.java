package pl.piotrkulma.bookstore.authorservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pl.piotrkulma.bookstore.authorservice.rest.AuthorInfoApi;
import pl.piotrkulma.bookstore.authorservice.service.AuthorService;
import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

@RestController
public class AuthorInfoApiImpl implements AuthorInfoApi {
	
	private AuthorService bookService;
	
	@Autowired
	public AuthorInfoApiImpl(AuthorService bookService) {
		this.bookService = bookService;
	}
	
	public ResponseEntity<List<AuthorDTO>> getAuthors() {
		return new ResponseEntity<List<AuthorDTO>>(bookService.getAllAuthors(), HttpStatus.OK);
	}
	
	public ResponseEntity<AuthorDTO> getAuthor(@PathVariable(name="id", required=true) long id) {
		return new ResponseEntity<AuthorDTO>(bookService.getAuthor(id), HttpStatus.OK);
	}
	
	public ResponseEntity<AuthorDTO> createAuthor(@RequestBody(required=true) AuthorDTO authorDTO) {
		return new ResponseEntity<AuthorDTO>(bookService.createAuthor(authorDTO), HttpStatus.OK);
	}
}
