package pl.piotrkulma.bookstore.authorservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.piotrkulma.bookstore.authorservice.converter.AuthorDTOToModelConverter;
import pl.piotrkulma.bookstore.authorservice.converter.AuthorToDTOConverter;
import pl.piotrkulma.bookstore.authorservice.service.AuthorService;
import pl.piotrkulma.bookstore.data.domain.book.Author;
import pl.piotrkulma.bookstore.data.domain.repository.AuthorRepository;
import pl.piotrkulma.bookstore.data.domain.repository.BookRepository;
import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

@Service
public class AuthorServiceImpl implements AuthorService {
	protected BookRepository bookRepository;
	protected AuthorRepository authorRepository;
	
	@Autowired
	public AuthorServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository) {
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
	}
	
	@Override
	public AuthorDTO getAuthor(long id) {
		return convertAuthor(authorRepository.findById(id).get());
	}
	
	@Override
	public List<AuthorDTO> getAllAuthors() {
		return authorRepository.findAll().stream()
				.map(a -> convertAuthor(a))
				.collect(Collectors.toList());
	}

	@Override
	public AuthorDTO createAuthor(AuthorDTO authorDTO) {
		Author model = AuthorDTOToModelConverter.getInstance().convert(true, authorDTO);
		authorRepository.save(model);
		return convertAuthor(model);
	}
	
	private AuthorDTO convertAuthor(Author author) {
		return AuthorToDTOConverter.getInstance().convert(author);
	}
}
