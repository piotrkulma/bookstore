package pl.piotrkulma.bookstore.authorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan({
	"pl.piotrkulma.bookstore.authorservice.service",
	"pl.piotrkulma.bookstore.authorservice.rest",
	"pl.piotrkulma.bookstore.webclient.feignclient"})

@EnableSwagger2
@Import( value = {
		pl.piotrkulma.bookstore.data.ApplicationConfig.class,
		pl.piotrkulma.bookstore.rest.errorhandle.ApplicationConfig.class
		})
@PropertySources({@PropertySource(value = "classpath:authorinfo.properties")})
@EnableAutoConfiguration
@EnableEurekaClient
@SpringBootApplication
public class ApplicationConfig {
	public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
	}
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                           
          .apis(RequestHandlerSelectors.basePackage("pl.piotrkulma.bookstore.authorservice.rest"))              
          .paths(PathSelectors.any())   
          .build();                                           
    }
}
