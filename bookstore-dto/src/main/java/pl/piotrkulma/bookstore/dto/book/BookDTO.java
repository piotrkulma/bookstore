package pl.piotrkulma.bookstore.dto.book;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BookDTO {
    private long id;
    private String name;
    private String isbn;
    private String language;
    private LocalDate publicationDate;    
    private int amount;
    private BigDecimal price;
    private List<Long> authorsIds;
    
    public BookDTO() {
    	authorsIds = new ArrayList<>();
    }
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public LocalDate getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(LocalDate publicationDate) {
		this.publicationDate = publicationDate;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public List<Long> getAuthorsIds() {
		return authorsIds;
	}
	public void setAuthorsIds(List<Long> authorsIds) {
		this.authorsIds = authorsIds;
	}
}
