package pl.piotrkulma.bookstore.dto.error;

import java.time.LocalDateTime;

public final class ApiErrorMessage {
	private ApiErrorMessageStatus status;
	private String message;
	private String debugInfo;
	private LocalDateTime timestamp;

	public static final class ApiErrorMessageBuilder {
		private ApiErrorMessageStatus status;
		private String message;
		private String debugInfo;
		private LocalDateTime timestamp;
		
		public static ApiErrorMessageBuilder getInstance() {
			return new ApiErrorMessageBuilder();
		}
		
		public ApiErrorMessage build() {
			ApiErrorMessage apiMessage = new ApiErrorMessage();
			
			if(timestamp == null) {
				timestamp = LocalDateTime.now();
			}
			
			apiMessage.status = status;
			apiMessage.message = message;
			apiMessage.debugInfo = debugInfo;		
			apiMessage.timestamp = timestamp;
			
			return apiMessage;
		}
		
		public ApiErrorMessageBuilder withStatus(ApiErrorMessageStatus status) {
			this.status = status;
			return this;
		}
		
		public ApiErrorMessageBuilder withMessage(String message) {
			this.message = message;
			return this;
		}
		
		public ApiErrorMessageBuilder withDebugInfo(String debugInfo) {
			this.debugInfo = debugInfo;
			return this;
		}
		
		public ApiErrorMessageBuilder withTimestamp(LocalDateTime timestamp) {
			this.timestamp = timestamp;
			return this;
		}
		
		private ApiErrorMessageBuilder() {
			
		}
	}

	public ApiErrorMessageStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public String getDebugInfo() {
		return debugInfo;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	
	private ApiErrorMessage() {
	}
}
