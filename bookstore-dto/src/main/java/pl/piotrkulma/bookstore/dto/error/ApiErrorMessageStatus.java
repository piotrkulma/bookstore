package pl.piotrkulma.bookstore.dto.error;

public enum ApiErrorMessageStatus {
	INTERNAL_ERROR("internal.error.status.code"),
	NO_ELEMENT_FOUND_ERROR("no.elemet.found.error.status.code"),
	ARGUMENT_TYPE_MISMATCH("argument.type.mismatch.code");
	
	public String code;
	
	ApiErrorMessageStatus(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
