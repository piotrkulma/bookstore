package pl.piotrkulma.bookstore.authorservice.rest;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

@RequestMapping("/api")
public interface AuthorInfoApi {
	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ResponseEntity<List<AuthorDTO>> getAuthors();
	
	@RequestMapping(value = "/author/{id}", method = RequestMethod.GET)
	public ResponseEntity<AuthorDTO> getAuthor(@PathVariable(name="id", required=true) long id);
	
	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public ResponseEntity<AuthorDTO> createAuthor(@RequestBody(required=true) AuthorDTO authorDTO);
}
