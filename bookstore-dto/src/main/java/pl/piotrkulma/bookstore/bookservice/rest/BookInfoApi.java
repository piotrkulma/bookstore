package pl.piotrkulma.bookstore.bookservice.rest;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.piotrkulma.bookstore.dto.book.BookDTO;

@RequestMapping("/api")
public interface BookInfoApi {
	@RequestMapping(value = "/book", method = RequestMethod.GET)
	public ResponseEntity<List<BookDTO>> getBooks();
	
	@RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
	public ResponseEntity<BookDTO> getBook(@PathVariable(name="id") long id);
	
	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public ResponseEntity<BookDTO> createBook(@RequestBody(required=true) BookDTO bookDTO);
}
