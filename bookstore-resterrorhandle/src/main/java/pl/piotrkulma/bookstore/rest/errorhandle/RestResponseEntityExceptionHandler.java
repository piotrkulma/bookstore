package pl.piotrkulma.bookstore.rest.errorhandle;

import java.util.Locale;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import pl.piotrkulma.bookstore.dto.error.ApiErrorMessage;
import pl.piotrkulma.bookstore.dto.error.ApiErrorMessageStatus;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {
	private MessageSource messageSource;
	
	@Value("${allow.debug.info.message:false}")
	private boolean allowDebugInfo;
	
	@Autowired
	public RestResponseEntityExceptionHandler(MessageSource messageSource) {
		super();
		this.messageSource = messageSource;
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<ApiErrorMessage> handleOtherError(Exception ex, WebRequest request) {
		return getResponseEntity(ex, ApiErrorMessageStatus.INTERNAL_ERROR, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({ NoSuchElementException.class })
	public ResponseEntity<ApiErrorMessage> handleNoElementFoundError(NoSuchElementException ex, WebRequest request) {
		return getResponseEntity(ex, ApiErrorMessageStatus.NO_ELEMENT_FOUND_ERROR, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<ApiErrorMessage> handleTypeMismatchError(MethodArgumentTypeMismatchException ex, WebRequest request) {
		return getResponseEntity(ex, ApiErrorMessageStatus.ARGUMENT_TYPE_MISMATCH, HttpStatus.NOT_FOUND);
	}
	
	protected ResponseEntity<ApiErrorMessage> getResponseEntity(Exception ex, ApiErrorMessageStatus apiErrorStatus, HttpStatus httpStatus) {
		return new ResponseEntity<ApiErrorMessage>(getApiErrorMessage(ex, apiErrorStatus), httpStatus);
	}
	
	protected ApiErrorMessage getApiErrorMessage(Exception ex, ApiErrorMessageStatus apiErrorStatus) {
		String debug = "";
		
		if(allowDebugInfo) {
			debug = ex.getMessage();
		}
		
		return ApiErrorMessage.ApiErrorMessageBuilder.getInstance()
				.withStatus(apiErrorStatus)
				.withDebugInfo(debug)
				.withMessage(getTextMessage(apiErrorStatus))
				.build();
	}
	
	protected String getTextMessage(ApiErrorMessageStatus status) {
		return messageSource.getMessage(status.getCode(), null, Locale.getDefault());
	}
}
