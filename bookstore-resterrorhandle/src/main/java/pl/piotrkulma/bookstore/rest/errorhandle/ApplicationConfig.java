package pl.piotrkulma.bookstore.rest.errorhandle;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@ComponentScan(basePackages={"pl.piotrkulma.bookstore.rest.errorhandle"})
@PropertySources({@PropertySource(value = "classpath:messages.properties")})
public class ApplicationConfig {

}
