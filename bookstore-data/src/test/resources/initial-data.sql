INSERT INTO public.author(id, birth_date, first_name, second_name) VALUES (1, DATE '1987-01-01', 'Adam', 'Mickiewicz');
INSERT INTO public.author(id, birth_date, first_name, second_name) VALUES (2, DATE '1900-04-01', 'Boles�aw', 'Prus');
INSERT INTO public.author(id, birth_date, first_name, second_name) VALUES (3, DATE '1945-01-09', 'Stephen', 'King');

INSERT INTO public.book(id, name, amount, isbn, price, language, publication_date) 
VALUES (1, 'Pan Tadeusz', 34, 'AAA-BBB-DDDD-EEEEE', 19.99, 'POLISH',  DATE '2018-01-09');

INSERT INTO public.book_authors(book_id, authors_id) VALUES(1, 1);
INSERT INTO public.book_authors(book_id, authors_id) VALUES(1, 2);

COMMIT;
