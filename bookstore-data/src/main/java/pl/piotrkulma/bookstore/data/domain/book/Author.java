package pl.piotrkulma.bookstore.data.domain.book;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "AUTHOR")
@SequenceGenerator(name="SEQ_AUTHOR_ID", initialValue=1, allocationSize=100)
public class Author {
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_AUTHOR_ID")
    @Column(name="ID")
    @Id private long id;
    
    @Column(name="FIRST_NAME")
    private String firstName;
    
    @Column(name="SECOND_NAME")
    private String secondName;
    
    @Column(name="BIRTH_DATE")
    private LocalDate birthDate;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}	
}
