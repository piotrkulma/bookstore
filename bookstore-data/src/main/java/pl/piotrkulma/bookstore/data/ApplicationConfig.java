package pl.piotrkulma.bookstore.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories
@ComponentScan(basePackages={"pl.piotrkulma.bookstore.data.domain.repository"})
@EntityScan(basePackages= {"pl.piotrkulma.bookstore.data.domain"})
@PropertySources({@PropertySource(value = "classpath:data.properties")})
public class ApplicationConfig {

}
