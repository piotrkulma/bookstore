package pl.piotrkulma.bookstore.data.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.piotrkulma.bookstore.data.domain.book.Author;

public interface AuthorRepository extends JpaRepository<Author, Long>{

}
