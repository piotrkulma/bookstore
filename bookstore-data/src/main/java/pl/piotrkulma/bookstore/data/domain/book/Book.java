package pl.piotrkulma.bookstore.data.domain.book;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BOOK")
@SequenceGenerator(name="SEQ_BOOK_ID", initialValue=1, allocationSize=100)
public class Book {
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_BOOK_ID")
    @Column(name="ID")
    @Id private long id;
    
    @Column(name="ISBN")
    private String isbn;
    
    @Column(name="NAME")
    private String name;
    
    @Column(name="LANGUAGE")
    @Enumerated(EnumType.STRING)
    private BookLanguage language;
    
    @Column(name="PUBLICATION_DATE")
    private LocalDate publicationDate;
    
    @Column(name="AMOUNT")
    private int amount;
    
    @Column(name="PRICE")
    private BigDecimal price;
    
    @JoinTable(name="book_authors", 
    		joinColumns=@JoinColumn(name = "book_id"), 
    		inverseJoinColumns=@JoinColumn(name="authors_id"))
    @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
    private List<Author> authors;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BookLanguage getLanguage() {
		return language;
	}

	public void setLanguage(BookLanguage language) {
		this.language = language;
	}

	public LocalDate getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(LocalDate publicationDate) {
		this.publicationDate = publicationDate;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
}
