package pl.piotrkulma.bookstore.data.domain.book;

public enum BookLanguage {
	POLISH,
	ENGLISH,
	GERMAN,
	FRENCH,
	SPAIN,
	RUSSIAN
}
