package pl.piotrkulma.bookstore.data.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.piotrkulma.bookstore.data.domain.book.Book;

public interface BookRepository extends JpaRepository<Book, Long>{
}
