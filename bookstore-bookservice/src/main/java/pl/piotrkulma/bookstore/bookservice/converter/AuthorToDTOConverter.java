package pl.piotrkulma.bookstore.bookservice.converter;

import pl.piotrkulma.bookstore.data.domain.book.Author;
import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

public final class AuthorToDTOConverter {
	private static AuthorToDTOConverter INSTANCE;
	
	private AuthorToDTOConverter() {
	}
	
	public static AuthorToDTOConverter getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new AuthorToDTOConverter();
		}
		
		return INSTANCE;
	}
	
	public AuthorDTO convert(Author author) {
		synchronized (INSTANCE) {
			return convertToDTO(author);
		}
	}
	
	private AuthorDTO convertToDTO(Author author) {
		AuthorDTO dto = new AuthorDTO();
		dto.setId(author.getId());
		dto.setFirstName(author.getFirstName());
		dto.setSecondName(author.getSecondName());
		dto.setBirthDate(author.getBirthDate());
		return dto;
	}
}
