package pl.piotrkulma.bookstore.bookservice.converter;

import pl.piotrkulma.bookstore.data.domain.book.Book;
import pl.piotrkulma.bookstore.dto.book.BookDTO;

public final class BookToDTOConverter {
	private static BookToDTOConverter INSTANCE;
	
	private BookToDTOConverter() {
	}
	
	public static BookToDTOConverter getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new BookToDTOConverter();
		}
		
		return INSTANCE;
	}
	
	public BookDTO convert(Book book) {
		synchronized (INSTANCE) {
			return convertToDTO(book);
		}
	}
	
	private BookDTO convertToDTO(Book book) {
		BookDTO dto = new BookDTO();
		dto.setId(book.getId());
		dto.setName(book.getName());
		dto.setAmount(book.getAmount());
		dto.setIsbn(book.getIsbn());
		dto.setLanguage(book.getLanguage().name());
		dto.setPrice(book.getPrice());
		dto.setPublicationDate(book.getPublicationDate());
		
		if(book.getAuthors() != null) {
			book.getAuthors().stream().forEach(a -> addAuthorId(a.getId(), dto));
		}
		
		return dto;
	}
	
	private void addAuthorId(Long id, BookDTO dto) {
		dto.getAuthorsIds().add(id);
	}
}
