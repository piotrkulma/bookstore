package pl.piotrkulma.bookstore.bookservice.service;

import java.util.List;

import pl.piotrkulma.bookstore.dto.book.BookDTO;

public interface BookService {
	BookDTO getBook(long id);
	List<BookDTO> getAllBooks();
	BookDTO createBook(BookDTO bookDTO);
}
