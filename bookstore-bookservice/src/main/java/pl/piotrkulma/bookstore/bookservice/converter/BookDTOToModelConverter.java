package pl.piotrkulma.bookstore.bookservice.converter;

import pl.piotrkulma.bookstore.data.domain.book.Book;
import pl.piotrkulma.bookstore.data.domain.book.BookLanguage;
import pl.piotrkulma.bookstore.dto.book.BookDTO;

public final class BookDTOToModelConverter {
	private static BookDTOToModelConverter INSTANCE;
	
	private BookDTOToModelConverter() {
	}
	
	public static BookDTOToModelConverter getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new BookDTOToModelConverter();
		}
		
		return INSTANCE;
	}
	
	public Book convert(boolean isNew, BookDTO book) {
		synchronized (INSTANCE) {
			return convertToDTO(isNew, book);
		}
	}
	
	private Book convertToDTO(boolean isNew, BookDTO dto) {
		Book book = new Book();
		if(isNew) {
			book.setId(dto.getId());
		}
		
		book.setAmount(dto.getAmount());
		book.setName(dto.getName());
		book.setIsbn(dto.getIsbn());
		book.setLanguage(BookLanguage.valueOf(dto.getLanguage()));
		book.setPrice(dto.getPrice());
		book.setPublicationDate(dto.getPublicationDate());
		
		return book;
	}
}
