package pl.piotrkulma.bookstore.bookservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pl.piotrkulma.bookstore.bookservice.service.BookService;
import pl.piotrkulma.bookstore.dto.book.BookDTO;

@RestController
public class BookInfoApiImpl implements BookInfoApi {
	
	private BookService bookService;
	
	@Autowired
	public BookInfoApiImpl(BookService bookService) {
		this.bookService = bookService;
	}
	
	public ResponseEntity<List<BookDTO>> getBooks() {
		return new ResponseEntity<List<BookDTO>>(bookService.getAllBooks(), HttpStatus.OK);
	}
	
	public ResponseEntity<BookDTO> getBook(@PathVariable(name="id") long id) {
		return new ResponseEntity<BookDTO>(bookService.getBook(id), HttpStatus.OK);
	}
	
	public ResponseEntity<BookDTO> createBook(@RequestBody(required=true) BookDTO bookDTO) {
		return new ResponseEntity<BookDTO>(bookService.createBook(bookDTO), HttpStatus.OK);
	}
}
