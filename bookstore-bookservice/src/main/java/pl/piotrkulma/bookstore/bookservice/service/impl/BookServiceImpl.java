package pl.piotrkulma.bookstore.bookservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.piotrkulma.bookstore.bookservice.converter.BookDTOToModelConverter;
import pl.piotrkulma.bookstore.bookservice.converter.BookToDTOConverter;
import pl.piotrkulma.bookstore.bookservice.service.BookService;
import pl.piotrkulma.bookstore.data.domain.book.Book;
import pl.piotrkulma.bookstore.data.domain.repository.AuthorRepository;
import pl.piotrkulma.bookstore.data.domain.repository.BookRepository;
import pl.piotrkulma.bookstore.dto.book.BookDTO;

@Service
public class BookServiceImpl implements BookService {
	protected BookRepository bookRepository;
	protected AuthorRepository authorRepository;
	
	@Autowired
	public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository) {
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
	}

	@Override
	public BookDTO getBook(long id) {
		return convertBook(bookRepository.findById(id).get());
	}
	
	@Override
	public List<BookDTO> getAllBooks() {
		return bookRepository.findAll().stream()
				.map(a -> convertBook(a))
				.collect(Collectors.toList());
	}
	
	@Override
	public BookDTO createBook(BookDTO bookDTO) {
		Book model = BookDTOToModelConverter.getInstance().convert(true, bookDTO);
		fillAuthors(model, bookDTO);
		bookRepository.save(model);
		return convertBook(model);
	}
	
	private void fillAuthors(Book model, BookDTO bookDTO) {
		if(bookDTO.getAuthorsIds().size() == 0) {
			return;
		}
		
		if(model.getAuthors() == null) {
			model.setAuthors(new ArrayList<>());
		}
		
		bookDTO.getAuthorsIds().stream()
		.forEach(id -> model.getAuthors().add(authorRepository.findById(id).get()));
	}
	
	private BookDTO convertBook(Book book) {
		return BookToDTOConverter.getInstance().convert(book);
	}
}
