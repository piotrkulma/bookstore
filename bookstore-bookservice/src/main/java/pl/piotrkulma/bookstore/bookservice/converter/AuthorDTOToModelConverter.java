package pl.piotrkulma.bookstore.bookservice.converter;

import pl.piotrkulma.bookstore.data.domain.book.Author;
import pl.piotrkulma.bookstore.dto.book.AuthorDTO;

public final class AuthorDTOToModelConverter {
	private static AuthorDTOToModelConverter INSTANCE;
	
	private AuthorDTOToModelConverter() {
	}
	
	public static AuthorDTOToModelConverter getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new AuthorDTOToModelConverter();
		}
		
		return INSTANCE;
	}
	
	public Author convert(boolean isNew, AuthorDTO author) {
		synchronized (INSTANCE) {
			return convertToDTO(isNew, author);
		}
	}
	
	private Author convertToDTO(boolean isNew, AuthorDTO dto) {
		Author author = new Author();
		if(isNew) {
			author.setId(dto.getId());
		}
		author.setFirstName(dto.getFirstName());
		author.setSecondName(dto.getSecondName());
		author.setBirthDate(dto.getBirthDate());
		return author;
	}
}
